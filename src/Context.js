import React, { Component } from 'react'
import {storeProducts,detailProduct} from './data'


const ProductContext = React.createContext()
class ProductProvider extends Component {
    state={
        product : storeProducts,
        ProductDetails : detailProduct
    }
    handleDetail = () => {
        console.log('hello from details')
    }
    addToCart = () => {
        console.log('hello from add To Cart')
    }
    render() {
        return (
            <ProductContext.Provider
                value = {{ 
                    ...this.state,
                    handleD:this.handleDetail,
                    cart:this.addToCart,
                }}>
                {this.props.children}
            </ProductContext.Provider>
        )
    }
}
const ProductConsumer = ProductContext.Consumer

export {ProductProvider,ProductConsumer};
