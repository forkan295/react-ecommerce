import React, { Component } from 'react'

class Default extends Component {
    render() {
        return (
            <div>
                <h1 className='text-white bg-danger text-center'>
                    page not found
                </h1>
            </div>
        )
    }
}

export default Default
