import React, { Component } from 'react'
import Product from './Product'
import Title from './Title'
import {ProductConsumer} from '../Context'


class ProductList extends Component {
    render() {
       
        return (
        <React.Fragment>
            <div className="my-4">
                <div className="container">
                    <Title name="Our" title="Product"/>                                               
                    <div className="row">
                      <ProductConsumer>
                          {(hello )=> {
                         return hello.product.map(product =>{
                             return (
                                 <Product  key= {product.id} product = {product}/>
                                  )
                                })
                              }}
                      </ProductConsumer>
                    </div>
                </div>
            </div>
        </React.Fragment>
        )
    }
}

export default ProductList
