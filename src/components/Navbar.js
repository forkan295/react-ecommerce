import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import styled from 'styled-components'

class Navbar extends Component {
    render() {
        return (
           <nav className="navbar navbar-expand-sm navbar-dark">
              <div className="container">
              <Link to='/'>
                <i className='fa fa-google fa-4x text-dark navbar-brand'><span className='text-info'>oogle</span></i>
               </Link>
               <ul className="navbar-nav align-items-center">
                   <li className="nav-item ml-5">
                       <Link to='/' className='nav-link text-dark'>Product</Link>
                   </li>
               </ul>
              <Link to = '/cart' className='ml-auto btn btn-danger'>
                  <Buttons className='fa fa-cart-plus'> Cart</Buttons>
              </Link>
              </div>
           </nav>
        )
    }
}
const Buttons = styled.span`
      text-transform:capitalize;
      
      
`

export default Navbar
