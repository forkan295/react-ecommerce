import React from 'react'

function Title({name,title}) {
    return (
        <div className='row'>
            <div className="col-10 mx-auto my-2 text-center">
               <h1>{name} - <strong className = "text-info">{title}</strong></h1>
            </div>
        </div>
    )
}

export default Title
