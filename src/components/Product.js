import React, { Component } from 'react'
import styled from 'styled-components'
import {Link} from 'react-router-dom'
import {ProductConsumer} from '../Context'

 class Product extends Component {
    render() {
        const {id,title,img,price,inCart} = this.props.product
       
        
        return (
            
          <ProductWraper className="col-9 col-md-5 col-lg-3 mx-auto m-3">
             <div className="card">
                 <div className="image-container p-4" onClick={console.log('hello')}>
                    <Link to ="/details">
                        <img src={img} alt="" className="cart-img-top img-fluid"/>
                    </Link>
                       <h5 className='text-center mb-3'>{title}</h5>
                       <p className='lead float-right'>${price}</p>
                    <button className='btn btn-danger' disabled={inCart ? true : false} onClick={()=>{console.log('added in cart')}}> {inCart ?"In Cart":(<i className='fa fa-cart-plus'></i>)}
                    </button>
                 </div>
             </div>
          </ProductWraper>
        )
    }
}

const ProductWraper = styled.div`

`

export default Product
